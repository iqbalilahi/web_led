<!doctype html>
<html lang="en">
<head>
  <?php require_once('main/head.php'); ?>
</head>
<body data-spy="scroll" data-target="#navbar-example">

  <div id="preloader"></div>

  <header>
    <?php require_once('main/header.php'); ?>
    <!-- header-area end -->
  </header>
  <!-- header end -->

  <!-- Start Slider Area -->
    <?php echo $contents;?>

  <!-- Start Footer bottom Area -->
  <footer>
    <?php require_once('main/footer.php'); ?>
  </footer>

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <?php require_once('main/js.php'); ?>
</body>

</html>
