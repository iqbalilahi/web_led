<!-- Start Slider Area -->
  <div id="home" class="slider-area">
    <div class="bend niceties preview-2">
      <div id="ensign-nivoslider" class="slides">
        <img src="<?php echo base_url(); ?>assets/img/slider/slider1.jpg" alt="slide1" title="#slider-direction-1" />
        <img src="<?php echo base_url(); ?>assets/img/slider/slider2.jpg" alt="slide2" title="#slider-direction-2" />
        <img src="<?php echo base_url(); ?>assets/img/slider/slider3.jpg" alt="slide3" title="#slider-direction-3" />
	<img src="<?php echo base_url(); ?>assets/img/slider/slider4.jpg" alt="slide4" title="#slider-direction-4" />
      </div>

      <!-- direction 1 -->
      <div id="slider-direction-1" class="slider-direction slider-one">
        <div class="container">
          <div class="row"> <!--
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
              </div>
            </div> -->
          </div>
        </div>
      </div>

      <!-- direction 2 -->
      <div id="slider-direction-2" class="slider-direction slider-two">
        <div class="container">
          <div class="row"> <!--
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content text-center">
				<div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                  <h1 class="title2">ECO FLUX</h1>
                </div>
                <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                  <h2 class="title1">LED LIGHTING</h2>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>

      <!-- direction 3 -->
      <div id="slider-direction-3" class="slider-direction slider-two">
        <div class="container">
          <div class="row"> <!--
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
              </div>
            </div> -->
          </div>
        </div>
      </div>
	  
	  <!-- direction 4 -->
      <div id="slider-direction-4" class="slider-direction slider-two">
        <div class="container">
          <div class="row"> <!--
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Slider Area -->