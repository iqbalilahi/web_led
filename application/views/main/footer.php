<!-- Start Footer bottom Area -->
<footer>
    <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <img src="<?php echo base_url(); ?>assets/img/footer.png" alt="footer" />
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <div class="footer-logo">
                                <h2><span>PowerLed</span> Indonesia</h2>
                            </div>
                            <p style="text-align:justify;">Is An Authorized Sole Distributor and Manufacturer's Representative
                                which supply all LED product "POWERLED" in Indonesia.</p>
                        </div>
                    </div>
                </div>
                <!-- end single footer -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <h5>PT POWERLED INDONESIA</h5>
                            <p>
                                Jl.Pinangsia Raya Blok F93 No.35,Jakarta 11180,Indonesia.</br >
                                <span>Telp:</span> +62 21 62203857</br>
                                <span>Website:</span> www.powerledindonesia.com</br>
                                <span>Email:</span> powerledindonesia@gmail.com
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-area-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="copyright text-center">
                        <p>
                            &copy;2018 Copyright <strong>PowerLedIndonesia</strong>. All Rights Reserved
                        </p>
                    </div>
                    <div class="credits">
                        Designed by <a href="#">Team Alpha</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>