
  <!-- Start About area -->
  <div id="about" class="about-area area-padding">
    <?php require_once('content_area/about_area.php'); ?>
  </div>
  <!-- End About area -->
    
  <!-- Start Service area -->
  <div id="services" class="services-area area-padding">
    <?php require_once('content_area/service_area.php'); ?>
  </div>
  <!-- End Service area -->

  <!-- our-skill-area start -->
  <div class="our-skill-area fix hidden-sm">
    <div class="test-overly"></div>
    <?php require_once('content_area/ourskill_area.php'); ?>
  </div>
  <!-- our-skill-area end -->

  <!-- Faq area start -->
  <div class="faq-area area-padding">
    <?php require_once('content_area/faq_area.php'); ?>
  </div>
  <!-- End Faq Area -->

  <!-- Start Wellcome Area -->
  <div class="wellcome-area">
    <?php require_once('content_area/welcome_area.php'); ?>
  </div>
  <!-- End Wellcome Area -->

  <!-- Start team Area -->
  <div id="team" class="our-team-area area-padding">
    <?php require_once('content_area/team_area.php'); ?>
  </div>
  <!-- End Team Area -->

  <!-- Start reviews Area -->
  <div class="reviews-area hidden-xs">
    <?php require_once('content_area/review_area.php'); ?>
  </div>
  <!-- End reviews Area -->

  <!-- Start portfolio Area -->
  <div id="portfolio" class="portfolio-area area-padding fix">
    <?php require_once('content_area/portofolio_area.php'); ?>
  </div>
  <!-- awesome-portfolio end -->
  <!-- start pricing area -->
  <div id="pricing" class="pricing-area area-padding">
    <?php require_once('content_area/pricing_area.php'); ?>
  </div>
  <!-- End pricing table area -->
  <!-- Start Testimonials -->
  <div class="testimonials-area">
    <div class="testi-inner area-padding">
      <div class="testi-overly"></div>
      <?php require_once('content_area/testi_area.php'); ?>
    </div>
  </div>
  <!-- End Testimonials -->
  <!-- Start Blog Area -->
  <div id="blog" class="blog-area">
    <div class="blog-inner area-padding">
      <div class="blog-overly"></div>
      <?php require_once('content_area/blog_area.php'); ?>
    </div>
  </div>
  <!-- End Blog -->
  <!-- Start Suscrive Area -->
  <div class="suscribe-area">
    <?php require_once('content_area/survice_area.php'); ?>
  </div>
  <!-- End Suscrive Area -->
  <!-- Start contact Area -->
  <div id="contact" class="contact-area">
    <div class="contact-inner area-padding">
      <div class="contact-overly"></div>
      <?php require_once('content_area/contact_area.php'); ?>
    </div>
  </div>
  <!-- End Contact Area -->