<header>
    <!-- header-area start -->
    <div id="sticker" class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <!-- Navigation -->
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- Brand -->
                            <a class="navbar-brand page-scroll sticky-logo">
                              <!-- <h1><span>ECO</span>FLUX</h1> -->
                                <!-- Uncomment below if you prefer to use an image logo -->
                                <img src="<?php echo base_url(); ?>assets/img/logopowerled.png" alt="logopowerled" title="eco flux" style="/*margin-left: -80px;*/">
                            </a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example" style="margin-top: 8px;">
                            <!-- <?php 
                                function main_menu() {
                                    $ci = & get_instance();
                                    $query = $ci->db->query("SELECT id_menu, nama_menu, link, id_parent FROM menu where aktif='Ya' AND position='Bottom' order by urutan");
                                    $menu = array('items' => array(),'parents' => array());
                                    foreach ($query->result() as $menus) {
                                        $menu['items'][$menus->id_menu] = $menus;
                                        $menu['parents'][$menus->id_parent][] = $menus->id_menu;
                                    }
                                    if ($menu) {
                                        $result = build_main_menu(0, $menu);
                                        return $result;
                                    }else{
                                        return FALSE;
                                    }
                                }

                                function build_main_menu($parent, $menu) {
                                    $html = "";

                                    if (isset($menu['parents'][$parent])) {
                                        if ($parent=='0'){
                                            $html .= "<ul class='nav navbar-nav navbar-right'>";
                                        }else{
                                            $html .= "<ul>";
                                        }
                                        foreach ($menu['parents'][$parent] as $itemId) {

                                            if (!isset($menu['parents'][$itemId])) {
                                                if(preg_match("/^http/", $menu['items'][$itemId]->link)) {
                                                    $html .= "<li class='active underborder'><a class='page-scroll' target='_BLANK' href='".$menu['items'][$itemId]->link."'>".$menu['items'][$itemId]->nama_menu."</a></li>";
                                                }else{
                                                    $html .= "<li><a class='page-scroll' href='".base_url().''.$menu['items'][$itemId]->link."'>".$menu['items'][$itemId]->nama_menu."</a></li>";
                                                }
                                            }
                                            if (isset($menu['parents'][$itemId])) {
                                                if(preg_match("/^http/", $menu['items'][$itemId]->link)) {
                                                    $html .= "<li class='page-scroll'><a target='_BLANK' href='".$menu['items'][$itemId]->link."'><span>".$menu['items'][$itemId]->nama_menu."</span></a>";
                                                }else{
                                                    $html .= "<li><a href='".base_url().''.$menu['items'][$itemId]->link."'><span>".$menu['items'][$itemId]->nama_menu."</span></a>";
                                                }
                                                $html .= build_main_menu($itemId, $menu);
                                                $html .= "</li>";
                                            }
                                        }
                                        $html .= "</ul>";
                                    }
                                    return $html;
                                }
                                echo main_menu();
                            ?>
                             -->
                          
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active underborder">
                                    <a class="page-scroll" href="<?php echo base_url(); ?>">Home</a>
                                </li>


                                <li class="dropdown">
                                    <a class="page-scroll" style="cursor:pointer;">About Us <b class="caret"></b></a>
                                    <ul class="dropdown-content">
                                        <!-- <li>
                                            <a class="" href="<?php echo base_url(); ?>Story/brief_story" style="font-size:90%; text-decoration:none;">Brief Story</a>
                                        </li> -->
                                        <li>
                                            <a class="" href="<?php echo base_url(); ?>About/company_profile" style="font-size:90%; text-decoration:none;">Company Profile</a>
                                        </li>
                                        <li>
                                            <a class="" href="<?php echo base_url(); ?>About/visi_misi" style="font-size:90%; text-decoration:none;">Vision and Mission</a>
                                        </li>
                                        <li>
                                            <a class="" href="<?php echo base_url(); ?>About/sertifikat" style="font-size:90%; text-decoration:none;">Certification</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="page-scroll" style="cursor:pointer;">Products <b class="caret"></b></a>
                                    <ul class="dropdown-content">
                                        <li>
                                            <a class="" href="<?php echo base_url(); ?>Product/Indoor" style="font-size:90%; text-decoration:none;">Indoor Lighting</a>
                                        </li>
                                        <li>
                                            <a class="" href="<?php echo base_url(); ?>Product/Outdoor" style="font-size:90%; text-decoration:none;">Outdoor Lighting</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- <li>
                                    <a class="page-scroll" href="<?php echo base_url(); ?>Client/client1">Clients</a>
                                </li> -->
                                <!-- <li>
                                    <a class="page-scroll" href="<?php echo base_url(); ?>New_p/new_products">News</a>
                                </li> -->
                                <li>
                                    <a class="page-scroll" href="<?php echo base_url(); ?>Contact/contact_us">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                        <!-- navbar-collapse -->
                    </nav>
                    <!-- END: Navigation -->
                </div>
            </div>
        </div>
    </div>
    <!-- header-area end -->
</header>
<!-- header end -->