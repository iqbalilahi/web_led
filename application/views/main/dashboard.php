<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Eco Flux Led Lighting</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url(); ?>assets/img/favicon.png" rel="icon">
  <link href="<?php echo base_url(); ?>assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url(); ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url(); ?>assets/lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/lib/owlcarousel/owl.carousel.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/lib/owlcarousel/owl.transitions.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/lib/venobox/venobox.css" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="<?php echo base_url(); ?>assets/css/nivo-slider-theme.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/styleclient.css" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#navbar-example">

  <div id="preloader"></div>

  <header>
    <!-- header-area start -->
    <div id="sticker" class="header-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">

            <!-- Navigation -->
            <nav class="navbar navbar-default">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                <!-- Brand -->
                <a class="navbar-brand page-scroll sticky-logo">
                  <!-- <h1><span>ECO</span>FLUX</h1> -->
                  <!-- Uncomment below if you prefer to use an image logo -->
                  <img src="img/logo.png" alt="eco flux" title="eco flux">
				</a>
              </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                <ul class="nav navbar-nav navbar-right">
                  <li class="active underborder">
                    <a class="page-scroll" href="#home">Home</a>
                  </li>
                  <li class="dropdown">
                    <a class="page-scroll" style="cursor:pointer;">About Us <b class="caret"></b></a>
					<ul class="dropdown-content">
						<li>
							<a class="" href="#" style="font-size:90%; text-decoration:none;">Brief Story</a>
						</li>
						<li>
							<a class="" href="#" style="font-size:90%; text-decoration:none;">Vision and Mission</a>
						</li>
						<li>
							<a class="" href="#" style="font-size:90%; text-decoration:none;">Certification</a>
						</li>
					</ul>
                  </li>
				  <li class="dropdown">
                    <a class="page-scroll" style="cursor:pointer;">Products <b class="caret"></b></a>
					<ul class="dropdown-content">
						<li>
							<a class="" href="#" style="font-size:90%; text-decoration:none;">Indoor Lighting</a>
						</li>
						<li>
							<a class="" href="#" style="font-size:90%; text-decoration:none;">Outdoor Lighting</a>
						</li>
					</ul>
                  </li>
                  <li>
                    <a class="page-scroll" href="#team">Clients</a>
                  </li>
                  <li>
                    <a class="page-scroll" href="#portfolio">News</a>
                  </li>
                  <li>
                    <a class="page-scroll" href="#contact">Contact Us</a>
                  </li>
                </ul>
              </div>
              <!-- navbar-collapse -->
            </nav>
            <!-- END: Navigation -->
          </div>
        </div>
      </div>
    </div>
    <!-- header-area end -->
  </header>
  <!-- header end -->

  <!-- Start Slider Area -->
  <div id="home" class="slider-area">
    <div class="bend niceties preview-2">
      <div id="ensign-nivoslider" class="slides">
        <img src="<?php echo base_url(); ?>assets/img/slider/slider1.jpg" alt="slide1" title="#slider-direction-1" />
        <img src="<?php echo base_url(); ?>assets/img/slider/slider2.jpg" alt="slide2" title="#slider-direction-2" />
        <img src="<?php echo base_url(); ?>assets/img/slider/slider3.jpg" alt="slide3" title="#slider-direction-3" />
		<img src="<?php echo base_url(); ?>assets/img/slider/slider4.jpg" alt="slide4" title="#slider-direction-4" />
      </div>

      <!-- direction 1 -->
      <div id="slider-direction-1" class="slider-direction slider-one">
        <div class="container">
          <div class="row"> <!--
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
              </div>
            </div> -->
          </div>
        </div>
      </div>

      <!-- direction 2 -->
      <div id="slider-direction-2" class="slider-direction slider-two">
        <div class="container">
          <div class="row"> <!--
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content text-center">
				<div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                  <h1 class="title2">ECO FLUX</h1>
                </div>
                <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                  <h2 class="title1">LED LIGHTING</h2>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>

      <!-- direction 3 -->
      <div id="slider-direction-3" class="slider-direction slider-two">
        <div class="container">
          <div class="row"> <!--
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
              </div>
            </div> -->
          </div>
        </div>
      </div>
	  
	  <!-- direction 4 -->
      <div id="slider-direction-4" class="slider-direction slider-two">
        <div class="container">
          <div class="row"> <!--
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Slider Area -->
  
  <!-- Start Service area -->
  <div id="services" class="services-area area-padding" style="margin:-50px 0px -20px 0px">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline services-head text-center">
            <h2>Lighting Solutions For Better Environment</h2>
          </div>
        </div>
      </div>
      <div class="row text-center">
        <div class="services-contents">
          <!-- Start Left services -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon">
					<i class="fa fa-handshake-o"></i>
				</a>
                  <h4>Eco Friendly</h4>
                  <p>
                    LED lights contain no toxic materials and chemicals like mercury and boron that are dangerous for the environment. It is 100% recyclable.
                  </p>
                </div>
              </div>
              <!-- end about-details -->
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon">
						<i class="fa fa-recycle"></i>
					</a>
                  <h4>Energy and Cost Efficiency</h4>
                  <p>
                    80-90% more efficient than traditional lighting (i.e. incandescent light bulbs – lampu pijar).
                  </p>
                </div>
              </div>
              <!-- end about-details -->
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <!-- end col-md-4 -->
            <div class=" about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon">
						<i class="fa fa-hourglass"></i>
					</a>
                  <h4>Long Life</h4>
                  <p>
                    LED has an average of 50,000 hours lifetime while traditional lighting has 10,000 hours average lifetime.
                  </p>
                </div>
              </div>
              <!-- end about-details -->
            </div>
          </div>
          <!-- End Left services -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Service area -->
    
  <!-- Start Testimonials -->
  <div class="testimonials-area">
    <div class="testi-inner area-padding">
      <div class="testi-overly"></div>
	  <h2 style="text-align:center;margin-top:-30px;color:#ffffff;">Our Clients</h2>
	   <section class="customer-logos">
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client1.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client2.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client3.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client4.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client5.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client6.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client7.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client8.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client9.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client10.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client11.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client12.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client13.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client14.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client15.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client16.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client17.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client18.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client19.jpg"></div>
		  <div><img src="<?php echo base_url(); ?>assets/img/clients/client20.jpg"></div>
	   </section>
    </div>
  </div>
  <!-- End Testimonials -->

  <!-- Start portfolio Area -->
  <div id="portfolio" class="portfolio-area area-padding fix" style="margin-bottom:-30px; padding-top:-25px;">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Products</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- Start Portfolio -page -->
        <div class="awesome-project-content">
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 design development">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="<?php echo base_url(); ?>assets/img/products/1.jpg" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="<?php echo base_url(); ?>assets/img/products/1.jpg">
                      <h4>LED Bulb Lighting</h4>
                      <span>Indoor Lighting</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 photo">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="<?php echo base_url(); ?>assets/img/products/2.jpg" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="<?php echo base_url(); ?>assets/img/products/2.jpg">
                      <h4>LED High Bay Light</h4>
                      <span>Indoor Lighting</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 design">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="<?php echo base_url(); ?>assets/img/products/3.jpg" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="<?php echo base_url(); ?>assets/img/products/3.jpg">
                      <h4>LED Panel Light</h4>
                      <span>Indoor Lighting</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 photo development">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="<?php echo base_url(); ?>assets/img/products/4.jpg" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="<?php echo base_url(); ?>assets/img/products/4.jpg">
                      <h4>LED Tube Light</h4>
                      <span>Indoor Lighting</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 development">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="<?php echo base_url(); ?>assets/img/products/5.jpg" alt="" /></a>
                <div class="add-actions text-center text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="<?php echo base_url(); ?>assets/img/products/5.jpg">
                      <h4>LED Strip Light</h4>
                      <span>Indoor Lighting</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 design photo">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="<?php echo base_url(); ?>assets/img/products/6.jpg" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="<?php echo base_url(); ?>assets/img/products/6.jpg">
                      <h4>LED Par Light</h4>
                      <span>Indoor Lighting</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
        </div>
      </div>
    </div>
  </div>
  <!-- awesome-portfolio end -->

  <!-- Start Footer bottom Area -->
  <footer>
    <div class="footer-area" style="margin:-50px 0px -65px 0px">
      <div class="container">
        <div class="row">
		<div class="col-md-3 col-sm-3 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
				<img src="<?php echo base_url(); ?>assets/img/footer.png" alt="footer" />
              </div>
            </div>
          </div>
          <div class="col-md-5 col-sm-5 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
                <div class="footer-logo" style="margin-bottom:-10px;">
                  <h2><span>Eco</span> Flux</h2>
                </div>
                <p style="text-align:justify;">Is a trademark of Premium High Quality LED Lighting, with Patents right Internationally including Indonesia, 
				which belongs to PT. Famindo International Multi Gemilang. PT. Famindo International Multi Gemilang was established in 2003, a local company 
				which doing Trading and Distribution.</p>
              </div>
            </div>
          </div>
          <!-- end single footer -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer-content">
              <div class="footer-head" style="text-align:justify;">
                <h5 style="font-size:130%;">PT. Famindo International Multi Gemilang</h5>
                <p>
                  Royal Sunter Blok F No. 9, jl. Danau Sunter Selatan, Jakarta Utara – 14350, Indonesia.</br >
				  <span>Tel:</span> +6221-6515945</br>
				  <span>Fax:</span> +6221-6515977</br>
				  <span>Email:</span> info@ecoflux-lighting.com
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy;2018 Copyright <strong>EcoFlux</strong>. All Rights Reserved
              </p>
            </div>
            <div class="credits">
              Designed by <a href="#">Uknwlh</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/venobox/venobox.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/knob/jquery.knob.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/parallax/parallax.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/lib/appear/jquery.appear.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/slick.js"></script>


  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url(); ?>assets/contactform/contactform.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
  <script type='text/javascript'>
	$(document).ready(function(){
		$('.customer-logos').slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 1500,
			arrows: false,
			dots: false,
			pauseOnHover: false,
			responsive: [{
				breakpoint: 768,
				settings: {
					slidesToShow: 4
				}
			}, {
				breakpoint: 520,
				settings: {
					slidesToShow: 3
				}
			}]
		});
	});
  </script>
</body>

</html>
