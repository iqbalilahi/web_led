<div id="about" class="about-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <h2>Brief Story</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- single-well start-->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="well-left">
                    <div class="single-well">
                        <a href="#">
                            <h4 class="sec-head">Visi</h4>
                        </a>
                        <p>
                            "POWERLED is a trademark of Premium High Quality LED Lighting, 
with Patents right National in Indonesia, which belongs to PT.
 Powerled Indonesia. PT. Powerled Indonesia  was established in 2009, 
a local company which doing Trading and Distribution. 
Managed by their professional team and keep growing and spreading their business in Indonesia.
PT Powerled Indonesia is an Authorized Sole Distributor and Manufacturer's 
Representative which supply all LED Product with Brand "POWERLED" in Indonesia. 
PT. Powerled Indonesia also expands business line in High-Tech sector, specifically in LED Lighting sector.
To support the growth of LED Lighting business line, PT.
Powerled Indonesia cooperate with a well experienced LED Lighting Factory.
With a very solid and wide distribution channel, PT.
Powerled Indonesia could widely spread in plenty areas of Indonesia, especially in Industrial Sector."
                        </p>

                    </div>
                </div>
            </div>
            <!-- single-well end-->
        </div>
    </div>
</div>