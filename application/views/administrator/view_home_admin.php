  <a style='color:#000' href='<?php echo base_url().$this->uri->segment(1); ?>/productpage'>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Product</span>
        <?php $jmla = $this->model_app->view('productpage')->num_rows(); ?>
        <span class="info-box-number"><?php echo $jmla; ?></span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
  </div><!-- /.col -->
  </a>

  <a style='color:#000' href='<?php echo base_url().$this->uri->segment(1); ?>/clientslider'>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-green"><i class="fa fa-file"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Cleint</span>
        <?php $jmlb = $this->model_app->view('clients_img')->num_rows(); ?>
        <span class="info-box-number"><?php echo $jmlb; ?></span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
  </div><!-- /.col -->
  </a>

  <a style='color:#000' href='<?php echo base_url().$this->uri->segment(1); ?>/iklanatas'>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Slider</span>
        <?php $jmlc = $this->model_app->view('slider')->num_rows(); ?>
        <span class="info-box-number"><?php echo $jmlc; ?></span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
  </div><!-- /.col -->
  </a>

  <a style='color:#000' href='<?php echo base_url().$this->uri->segment(1); ?>/manajemenuser'>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Users</span>
        <?php $jmld = $this->model_app->view('users')->num_rows(); ?>
        <span class="info-box-number"><?php echo $jmld; ?></span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
  </div><!-- /.col -->
  </a>

<section class="col-lg-12 connectedSortable">
  <div class='box'>
    <div class='box-header'>
      <h3 class='box-title'>Application Buttons</h3>
    </div>
    <div class='box-body'>
      <p>Silahkan klik menu pilihan yang berada di sebelah kiri untuk mengelola konten website anda 
          atau pilih ikon-ikon pada Control Panel di bawah ini : </p>
      
      <a href="<?php echo base_url().$this->uri->segment(1); ?>/iklanatas" class='btn btn-app'><i class='fa fa-home'></i> Slider Home</a>
      <a href="<?php echo base_url().$this->uri->segment(1); ?>/clientslider" class='btn btn-app'><i class='fa fa-image'></i> Client Slider</a>
      <a href="<?php echo base_url().$this->uri->segment(1); ?>/producthome" class='btn btn-app'><i class='fa fa-dropbox'></i> Product Home</a>
      <a href="<?php echo base_url().$this->uri->segment(1); ?>/productpage" class='btn btn-app'><i class='fa fa-indent'></i> Product Page</a>
      <a href="<?php echo base_url().$this->uri->segment(1); ?>/visi_misi" class='btn btn-app'><i class='fa fa-bullseye'></i> Visi Misi</a>
      <a href="<?php echo base_url().$this->uri->segment(1); ?>/sertifikat" class='btn btn-app'><i class='fa fa-file'></i> Sertifikat</a>
      <a href="<?php echo base_url().$this->uri->segment(1); ?>/manajemenuser" class='btn btn-app'><i class='fa fa-users'></i> Users</a>
    </div>
  </div>
</section><!-- /.Left col -->


