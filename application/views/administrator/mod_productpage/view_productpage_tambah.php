<?php 
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Tambah Product Page</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart($this->uri->segment(1).'/tambah_productpage',$attributes); 
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value=''>

                    <tr><th width='120px' scope='row'>Nama Product</th>    <td><input type='text' class='form-control' name='a' required></td></tr>
                    <tr><th width='120px' scope='row'>Ringkasan</th>    <td><textarea id='editor1' class='form-control' name='b' required></textarea></td></tr>
                    <tr><th width='120px' scope='row'>Kategori</th>    <td>";
                    
                  echo "
                                                                          <input type='radio' value ='indoor'  name='d' >Indoor
                                                                          <input type='radio' value ='outdoor'  name='d' >Outdoor
                                                                        </td></tr> 
                    <tr><th width='120px' scope='row'>Gambar</th>    <td><input type='file' class='form-control' name='c'></td></tr>
                    <tr><th ></th>    <td>Resolusi 600 x 600</td></tr>
                  </tbody>
                  </table>
                </div>
              
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Tambahkan</button>
                    <a href='".base_url().$this->uri->segment(1)."/productpage'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                    
                  </div>
            </div></div></div>";
            echo form_close();
