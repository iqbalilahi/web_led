<?php 
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Edit Visi Misi</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart($this->uri->segment(1).'/edit_visi_misi',$attributes); 
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value='$rows[id_visi_misi]'>
                    <tr><th width='120px' scope='row'>Visi</th>             <td><textarea id='editor1' class='form-control' name='a' style='height:260px' required>$rows[visi]</textarea></td></tr>
                    <tr><th width='120px' scope='row'>Misi</th>     <td><textarea id='editor2' class='form-control' name='b' style='height:260px' required>$rows[misi]</textarea></td></tr>
               ";
                     echo "
                  </tbody>
                  </table>
                </div>
              
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Update</button>
                    <a href='".base_url().$this->uri->segment(1)."/visi_misi'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                    
                  </div>
            </div></div></div>";
            echo form_close();
