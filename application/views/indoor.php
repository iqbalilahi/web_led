<!-- End Slider Area -->
<div id="blog" class="blog-area">
    <div class="blog-inner area-padding">
        <div class="blog-overly"></div>
        <div class="container ">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Indoor Product</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- Start Left Blog -->
                <?php
						$indoor = $this->model_utama->view_indoor('productpage','indoor');
						foreach ($indoor->result_array() as $b) {
						if ($b != ''){
							
								echo  "<div class='col-md-4 col-sm-4 col-xs-12'>
                                           <div class='single-blog'>
                                                <div class='single-blog-img'>
                                              <a><img src='".base_url()."assets/img/productpage/$b[gambar]'></a>
                                        </div>
                                        <div class='section-body text-center'>
                                             <h4>
                                              <a href='#'><b>$b[nama_product]</b></a>
                                              </h4>
                                        </div>
                                              <div class='blog-text'>
                                              <p>$b[isi]</p>
                                        </div>
                                        </div>
                                    </div>";

							} else {
								echo "Kosong";
							}
						}
						
		        	?>
              
                <!-- End Left Blog-->
               
            </div>
        </div>
    </div>
</div>