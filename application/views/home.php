<div id="home" class="slider-area">

    <div id="home" class="slider-area">
        <div class="bend niceties preview-2">
        <!-- slider pada home -->
             <div id="ensign-nivoslider" class="slides">

				<?php
					  $slider = $this->model_utama->view('slider');
					  foreach ($slider->result_array() as $b) {
						$string = $b['gambar'];
						if ($b['gambar'] != ''){
							if(preg_match("/swf\z/i", $string)) {
								echo "<img src='".base_url()."assets/img/slider/$b[gambar]' alt='slide1' title='#slider-direction-1'>";
							} else {
								echo "<a href='$b[url]'><img width='100%' src='".base_url()."assets/img/slider/$b[gambar]' alt='slide1' /></a>";
							}
						}
					  }
				?>
              
            </div> 
            <div id="slider-direction-1" class="slider-direction slider-one">
                <div class="container">
                    <!-- <div class="row"> 
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-content">
						
                        </div>
                      </div> -->
                    </div>
                </div>
            </div>

            <!-- direction 2 -->
            <div id="slider-direction-2" class="slider-direction slider-two">
                <div class="container">
                    <div class="row"> <!--
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-content text-center">
                                          <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                            <h1 class="title2">ECO FLUX</h1>
                          </div>
                          <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                            <h2 class="title1">LED LIGHTING</h2>
                          </div>
                        </div>
                      </div> -->
                    </div>
                </div>
            </div>

            <!-- direction 3 -->
            <div id="slider-direction-3" class="slider-direction slider-two">
                <div class="container">
                    <div class="row"> <!--
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-content">
                        </div>
                      </div> -->
                    </div>
                </div>
            </div>

            <!-- direction 4 -->
            <div id="slider-direction-4" class="slider-direction slider-two">
                <div class="container">
                    <div class="row"> <!--
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-content">
                        </div>
                      </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="services" class="services-area area-padding" style="margin:-50px 0px -20px 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline services-head text-center">
                    <h2>Lighting Solutions For Better Environment</h2>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="services-contents">
                <!-- Start Left services -->
                <div class="col-md-4 col-sm-4 col-xs-12 totop">
                    <div class="about-move">
                        <div class="services-details">
                            <div class="single-services">
                                <a class="services-icon">
                                    <i class="fa fa-handshake-o"></i>
                                </a>
                                <h4>Partners</h4>
                                <p>
                                    Kami didukung oleh partner kami dalam membangun perusahaan <b>"POWERLED"</b> sesuai dengan Visi Dan Misi Perusahaan.
                                </p>
                            </div>
                        </div>
                        <!-- end about-details -->
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="about-move">
                        <div class="services-details">
                            <div class="single-services">
                                <a class="services-icon">
                                    <i class="fa fa-recycle"></i>
                                </a>
                                <h4>Go Green</h4>
                                <p>
                                    Cara termudah melakukan <b>"Go Green"</b> adalah Mengurangi pemakaian energi listrik secara berlebihan.
                                    Gunakan bila perlu dan matikan bila tidak diperlukan.
                                </p>
                            </div>
                        </div>
                        <!-- end about-details -->
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <!-- end col-md-4 -->
                    <div class=" about-move">
                        <div class="services-details">
                            <div class="single-services">
                                <a class="services-icon">
                                    <i class="fa fa-hourglass"></i>
                                </a>
                                <h4>Our Works</h4>
                                <p>
                                    <b>Kelebihan</b> produk kami adalah efesien, hemat energi, kualitas tinggi, kinerja yang baik,
                                    radiasi rendah, tahan lama, aman diapakai, tidak mengandung "Merkuri",
                                    tidak silau, bisa di "Service" dan tersedia dalam beraneka warna.
                                </p>
                            </div>
                        </div>
                        <!-- end about-details -->
                    </div>
                </div>
                <!-- End Left services -->
            </div>
        </div>
    </div>
</div>
<div class="testimonials-area">
    <div class="testi-inner area-padding">
      <div class="testi-overly"></div>
	  <h2>Our Clients</h2>
	   <section class="customer-logos">
	   <!--looping our clients -->
			<?php
						$clients = $this->model_utama->view('clients_img');
						foreach ($clients->result_array() as $b) {
						$string = $b['gambar'];
						if ($b['gambar'] != ''){
							if(preg_match("/swf\z/i", $string)) {
								echo "<img src='".base_url()."assets/img/clients/$b[gambar]' alt='slide1' title='#slider-direction-1'>";
							} else {
								echo "<a href='$b[url]'><img width='100%' src='".base_url()."assets/img/clients/$b[gambar]' alt='slide1' /></a>";
							}
						}
						}
			?>

	   </section>
    </div>
  </div>
  <!-- End Testimonials -->
  <div id="portfolio" class="portfolio-area area-padding fix" style="margin-bottom:-30px; padding-top:-25px;">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Products</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- Start Portfolio -page -->
        <div class="awesome-project-content">
          <!-- single-awesome-project start -->
			<?php
				$products = $this->model_utama->view_product_home('product');
				foreach ($products->result_array() as $b) {
				$string = $b['gambar'];
					if ($b['gambar'] != ''){
						
							echo "<div class='col-md-4 col-sm-4 col-xs-12 design development'>
									<div class='single-awesome-project'>
										<div class='awesome-img'>
											<img src='".base_url()."assets/img/products/$b[gambar]' alt='slide1' title='#slider-direction-1'>
												<div class='add-actions text-center'>
													<div class='project-dec'>
														<a class='venobox' data-gall='myGallery' href='".base_url()."assets/img/products/5.jpg'>
															<h4>$b[nama_product]</h4>
																<span>$b[keterangan]</span>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>";		
					}
				}
			?>
        </div>
      </div>
    </div>
  </div>
  <!-- awesome-portfolio end -->