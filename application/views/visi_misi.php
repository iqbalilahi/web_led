<div id="about" class="about-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <h2>Visi - Misi PowerLed Indonesia</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- single-well start-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-middle">
                    <div class="single-well">
                       
                    <?php
						$clients = $this->model_utama->view_visi_misi('visi_misi');
						foreach ($clients->result_array() as $b) {
						if ($b != ''){
							
								echo "<a href='#'>
                                     <h4 class='sec-head'>Visi</h4>
                                         </a>"."$b[visi]";
							} else {
								echo "Kosong";
							}
						}
						
		        	?>
                       
                        <!-- <a href="#">
                            <h4 class="sec-head">Visi</h4>
                        </a>
                        <p>
                            "Memperluas jaringan dan market produk secara merata."
                        </p> -->

                    </div>
                </div>
            </div>
            <!-- single-well end-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-middle">
                    <div class="single-well">

                    <?php
						$clients = $this->model_utama->view_visi_misi('visi_misi');
						foreach ($clients->result_array() as $b) {
						if ($b != ''){
							
								echo "<a href='#'>
                                     <h4 class='sec-head'>Misi</h4>
                                         </a>"."$b[misi]";
							} else {
								echo "Kosong";
							}
						}
						
		        	?>
                       
                        <!-- <p>
                            "Selalu memberikan pelayanan yang terbaik kepada customer, serta menjaga kualitas produk dengan memperhatikan permintaan pasar lampu yang selalu UP TO DATE
                            (mengikuti perkembangan zaman) sehingga selalu mengutamakan customer dalam menjalankan ussaha kami".
                        </p> -->
                        <!--              <ul>
                                        <li>
                                          <i class="fa fa-check"></i> Interior design Package
                                        </li>
                                        <li>
                                          <i class="fa fa-check"></i> Building House
                                        </li>
                                        <li>
                                          <i class="fa fa-check"></i> Reparing of Residentail Roof
                                        </li>
                                        <li>
                                          <i class="fa fa-check"></i> Renovaion of Commercial Office
                                        </li>
                                        <li>
                                          <i class="fa fa-check"></i> Make Quality Products
                                        </li>
                                      </ul>-->
                    </div>
                </div>
            </div>
            <!-- End col-->
        </div>
    </div>
</div>