<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    public function visi_misi()
	{
            $this->template->load('template','visi_misi');
            //$this->load->view('main/dashboard');
        }
        public function sertifikat()
	{
            $this->template->load('template','sertifikat');
            //$this->load->view('main/dashboard');
        }
        public function company_profile(){
            $this->template->load('template','cp');
        }
}
