<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Administrator extends CI_Controller {
	function index(){
		if (isset($_POST['submit'])){
            if ($this->input->post() && (strtolower($this->input->post('security_code')) == strtolower($this->session->userdata('mycaptcha')))) {
                $username = $this->input->post('a');
    			$password = md5($this->input->post('b'));
    			$cek = $this->model_app->cek_login($username,$password,'users');
    		    $row = $cek->row_array();
    		    $total = $cek->num_rows();
    			if ($total > 0){
    				$this->session->set_userdata('upload_image_file_manager',true);
    				$this->session->set_userdata(array('username'=>$row['username'],
    								   'level'=>$row['level'],
                                       'id_session'=>$row['id_session']));
    				redirect($this->uri->segment(1).'/home');
    			}else{
                    echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Username dan Password Salah!!</center></div>');
    				redirect($this->uri->segment(1).'/index');
    			}
            }else{
                echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Security Code salah!</center></div>');
                redirect($this->uri->segment(1).'/index');
            }
		}else{
            if ($this->session->level!=''){
              redirect('/home');
            }else{
                $this->load->helper('captcha');
                $vals = array(
                    'img_path'   => './captcha/',
                    'img_url'    => base_url().'captcha/',
                    'font_path' => './asset/Tahoma.ttf',
                    'font_size'     => 17,
                    'img_width'  => '320',
                    'img_height' => 33,
                    'border' => 0, 
                    'word_length'   => 5,
                    'expiration' => 7200
                );

                $cap = create_captcha($vals);
                $data['image'] = $cap['image'];
                $this->session->set_userdata('mycaptcha', $cap['word']);
    			$data['title'] = 'Administrator &rsaquo; Log In';
    			$this->load->view('administrator/view_login',$data);
            }
		}
    }
    	// Controller Modul Kategori Berita

	function visi_misi(){
		cek_session_akses('visi_misi',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('visi_misi','id_visi_misi','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('visi_misi',array('username'=>$this->session->username),'id_visi_misi','DESC');
        }
		$this->template->load('administrator/template','administrator/mod_visi_misi/view_visi_misi',$data);
	}
	function edit_visi_misi(){
		cek_session_akses('visi_misi',$this->session->id_session);
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$data = array('visi'=>$this->input->post('a'),
                        'username'=>$this->session->username,
                        'misi'=>$this->input->post('b')
                       );
			$where = array('id_visi_misi' => $this->input->post('id'));
			$this->model_app->update('visi_misi', $data, $where);
			redirect($this->uri->segment(1).'/visi_misi');
		}else{
            if ($this->session->level=='admin'){
                 $proses = $this->model_app->edit('visi_misi', array('id_visi_misi' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('visi_misi', array('id_visi_misi' => $id, 'username' => $this->session->username))->row_array();
            }
			$data = array('rows' => $proses);
			$this->template->load('administrator/template','administrator/mod_visi_misi/view_visi_misi_edit',$data);
		}
	}
    function sertifikat(){
		cek_session_akses('sertifikat',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('sertifikat','id_sertifikat','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('sertifikat',array('username'=>$this->session->username),'id_sertifikat','DESC');
        }
		$this->template->load('administrator/template','administrator/mod_sertifikat/view_sertifikat',$data);
	}

    function tambah_sertifikat(){
        cek_session_akses('sertifikat',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'assets/img/about/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                'username'=>$this->session->username,
                                'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                'username'=>$this->session->username,
                                'gambar'=>$hasil['file_name'],
                                'tgl_posting'=>date('Y-m-d'));
            }
            $this->model_app->insert('sertifikat',$data);  
            redirect($this->uri->segment(1).'/sertifikat');
        }else{
            $this->template->load('administrator/template','administrator/mod_sertifikat/view_sertifikat_tambah');
        }
    }
    
    function edit_sertifikat(){
        cek_session_akses('sertifikat',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'assets/img/products/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                'username'=>$this->session->username,
                                'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                'username'=>$this->session->username,
                                'gambar'=>$hasil['file_name'],
                                'tgl_posting'=>date('Y-m-d'));
            }
            $where = array('id_sertifikat' => $this->input->post('id'));
            $this->model_app->update('sertifikat', $data, $where);
            redirect($this->uri->segment(1).'/sertifikat');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('sertifikat', array('id_sertifikat' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('sertifikat', array('id_sertifikat' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_sertifikat/view_sertifikat_edit',$data);
        }
    }
    
    function delete_sertifikat(){
        cek_session_akses('sertifikat',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_sertifikat' => $this->uri->segment(3));
        }else{
            $id = array('id_sertifikat' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('sertifikat',$id);
        redirect($this->uri->segment(1).'/sertifikat');
    }


    function productpage(){
		cek_session_akses('productpage',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('productpage','id_product','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('productpage',array('username'=>$this->session->username),'id_product','DESC');
        }
		$this->template->load('administrator/template','administrator/mod_productpage/view_productpage',$data);
	}

    function tambah_productpage(){
        cek_session_akses('productpage',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'assets/img/productpage/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('nama_product'=>$this->db->escape_str($this->input->post('a')),
                                'isi'=>$this->input->post('b'),
                                'kategori'=>$this->input->post('d'),
                                'username'=>$this->session->username,
                                'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('nama_product'=>$this->db->escape_str($this->input->post('a')),
                                'isi'=>$this->input->post('b'),
                                'kategori'=>$this->input->post('d'),
                                'username'=>$this->session->username,
                                'gambar'=>$hasil['file_name'],
                                'tgl_posting'=>date('Y-m-d'));
            }
            $this->model_app->insert('productpage',$data);  
            redirect($this->uri->segment(1).'/productpage');
        }else{
            $this->template->load('administrator/template','administrator/mod_productpage/view_productpage_tambah');
        }
    }
    
    function edit_productpage(){
        cek_session_akses('producthome',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'assets/img/productpage/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('nama_product'=>$this->db->escape_str($this->input->post('a')),
                                'isi'=>$this->input->post('b'),
                                'kategori'=>$this->input->post('d'),
                                'username'=>$this->session->username,
                                'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('nama_product'=>$this->db->escape_str($this->input->post('a')),
                                'isi'=>$this->input->post('b'),
                                'kategori'=>$this->input->post('d'),
                                'username'=>$this->session->username,
                                'gambar'=>$hasil['file_name'],
                                'tgl_posting'=>date('Y-m-d'));
            }
            $where = array('id_product' => $this->input->post('id'));
            $this->model_app->update('productpage', $data, $where);
            redirect($this->uri->segment(1).'/productpage');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('productpage', array('id_product' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('productpage', array('id_product' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_productpage/view_productpage_edit',$data);
        }
    }
    
    function delete_productpage(){
        cek_session_akses('productpage',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_product' => $this->uri->segment(3));
        }else{
            $id = array('id_product' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('productpage',$id);
        redirect($this->uri->segment(1).'/productpage');
    }

 // Controller Modul clientslider

 function producthome(){
    cek_session_akses('producthome',$this->session->id_session);
    if ($this->session->level=='admin'){
        $data['record'] = $this->model_app->view_ordering('product','id_product','DESC');
    }else{
        $data['record'] = $this->model_app->view_where_ordering('product',array('username'=>$this->session->username),'id_product','DESC');
    }
    $this->template->load('administrator/template','administrator/mod_producthome/view_producthome',$data);
}

function tambah_producthome(){
    cek_session_akses('producthome',$this->session->id_session);
    if (isset($_POST['submit'])){
        $config['upload_path'] = 'assets/img/products/';
        $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
        $config['max_size'] = '3000'; // kb
        $this->load->library('upload', $config);
        $this->upload->do_upload('c');
        $hasil=$this->upload->data();
        if ($hasil['file_name']==''){
            $data = array('nama_product'=>$this->db->escape_str($this->input->post('a')),
                            'username'=>$this->session->username,
                            'url'=>$this->input->post('b'),
                            'keterangan'=>$this->input->post('d'),
                            'urutan'=>$this->input->post('e'),
                            'tgl_posting'=>date('Y-m-d'));
        }else{
            $data = array('nama_product'=>$this->db->escape_str($this->input->post('a')),
                            'username'=>$this->session->username,
                            'url'=>$this->input->post('b'),
                            'keterangan'=>$this->input->post('d'),
                            'urutan'=>$this->input->post('e'),
                            'gambar'=>$hasil['file_name'],
                            'tgl_posting'=>date('Y-m-d'));
        }
        $this->model_app->insert('product',$data);  
        redirect($this->uri->segment(1).'/producthome');
    }else{
        $this->template->load('administrator/template','administrator/mod_producthome/view_producthome_tambah');
    }
}

function edit_producthome(){
    cek_session_akses('producthome',$this->session->id_session);
    $id = $this->uri->segment(3);
    if (isset($_POST['submit'])){
        $config['upload_path'] = 'assets/img/products/';
        $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
        $config['max_size'] = '3000'; // kb
        $this->load->library('upload', $config);
        $this->upload->do_upload('c');
        $hasil=$this->upload->data();
        if ($hasil['file_name']==''){
            $data = array('nama_product'=>$this->db->escape_str($this->input->post('a')),
                            'username'=>$this->session->username,
                            'url'=>$this->input->post('b'),
                            'keterangan'=>$this->input->post('d'),
                            'tgl_posting'=>date('Y-m-d'));
        }else{
            $data = array('nama_product'=>$this->db->escape_str($this->input->post('a')),
                            'username'=>$this->session->username,
                            'url'=>$this->input->post('b'),
                            'keterangan'=>$this->input->post('d'),
                            'gambar'=>$hasil['file_name'],
                            'tgl_posting'=>date('Y-m-d'));
        }
        $where = array('id_product' => $this->input->post('id'));
        $this->model_app->update('product', $data, $where);
        redirect($this->uri->segment(1).'/producthome');
    }else{
        if ($this->session->level=='admin'){
            $proses = $this->model_app->edit('product', array('id_product' => $id))->row_array();
        }else{
            $proses = $this->model_app->edit('product', array('id_product' => $id, 'username' => $this->session->username))->row_array();
        }
        $data = array('rows' => $proses);
        $this->template->load('administrator/template','administrator/mod_producthome/view_producthome_edit',$data);
    }
}

function delete_producthome(){
    cek_session_akses('producthome',$this->session->id_session);
    if ($this->session->level=='admin'){
        $id = array('id_product' => $this->uri->segment(3));
    }else{
        $id = array('id_product' => $this->uri->segment(3), 'username'=>$this->session->username);
    }
    $this->model_app->delete('product',$id);
    redirect($this->uri->segment(1).'/producthome');
}

        // Controller Modul clientslider

        function clientslider(){
            cek_session_akses('clientslider',$this->session->id_session);
            if ($this->session->level=='admin'){
                $data['record'] = $this->model_app->view_ordering('clients_img','id_slider','DESC');
            }else{
                $data['record'] = $this->model_app->view_where_ordering('clients_img',array('username'=>$this->session->username),'id_slider','DESC');
            }
            $this->template->load('administrator/template','administrator/mod_clientslider/view_clientslider',$data);
        }
    
        function tambah_clientslider(){
            cek_session_akses('clientslider',$this->session->id_session);
            if (isset($_POST['submit'])){
                $config['upload_path'] = 'assets/img/clients/';
                $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
                $config['max_size'] = '3000'; // kb
                $this->load->library('upload', $config);
                $this->upload->do_upload('c');
                $hasil=$this->upload->data();
                if ($hasil['file_name']==''){
                    $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                    'username'=>$this->session->username,
                                    'url'=>$this->input->post('b'),
                                    'tgl_posting'=>date('Y-m-d'));
                }else{
                    $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                    'username'=>$this->session->username,
                                    'url'=>$this->input->post('b'),
                                    'gambar'=>$hasil['file_name'],
                                    'tgl_posting'=>date('Y-m-d'));
                }
                $this->model_app->insert('clients_img',$data);  
                redirect($this->uri->segment(1).'/clientslider');
            }else{
                $this->template->load('administrator/template','administrator/mod_clientslider/view_clientslider_tambah');
            }
        }
    
        function edit_clientslider(){
            cek_session_akses('clientslider',$this->session->id_session);
            $id = $this->uri->segment(3);
            if (isset($_POST['submit'])){
                $config['upload_path'] = 'assets/img/clients/';
                $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
                $config['max_size'] = '3000'; // kb
                $this->load->library('upload', $config);
                $this->upload->do_upload('c');
                $hasil=$this->upload->data();
                if ($hasil['file_name']==''){
                    $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                    'username'=>$this->session->username,
                                    'url'=>$this->input->post('b'),
                                    'tgl_posting'=>date('Y-m-d'));
                }else{
                    $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                    'username'=>$this->session->username,
                                    'url'=>$this->input->post('b'),
                                    'gambar'=>$hasil['file_name'],
                                    'tgl_posting'=>date('Y-m-d'));
                }
                $where = array('id_slider' => $this->input->post('id'));
                $this->model_app->update('clients_img', $data, $where);
                redirect($this->uri->segment(1).'/clientslider');
            }else{
                if ($this->session->level=='admin'){
                    $proses = $this->model_app->edit('clients_img', array('id_slider' => $id))->row_array();
                }else{
                    $proses = $this->model_app->edit('clients_img', array('id_slider' => $id, 'username' => $this->session->username))->row_array();
                }
                $data = array('rows' => $proses);
                $this->template->load('administrator/template','administrator/mod_clientslider/view_clientslider_edit',$data);
            }
        }
    
        function delete_clientslider(){
            cek_session_akses('clientslider',$this->session->id_session);
            if ($this->session->level=='admin'){
                $id = array('id_slider' => $this->uri->segment(3));
            }else{
                $id = array('id_slider' => $this->uri->segment(3), 'username'=>$this->session->username);
            }
            $this->model_app->delete('clients_img',$id);
            redirect($this->uri->segment(1).'/clientslider');
        }

   // Controller Modul Iklan Atas

   function iklanatas(){
    cek_session_akses('iklanatas',$this->session->id_session);
    if ($this->session->level=='admin'){
        $data['record'] = $this->model_app->view_ordering('slider','id_slider','DESC');
    }else{
        $data['record'] = $this->model_app->view_where_ordering('slider',array('username'=>$this->session->username),'id_slider','DESC');
    }
    $this->template->load('administrator/template','administrator/mod_iklanatas/view_iklanatas',$data);
}

function tambah_iklanatas(){
    cek_session_akses('iklanatas',$this->session->id_session);
    if (isset($_POST['submit'])){
        $config['upload_path'] = 'assets/img/slider/';
        $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
        $config['max_size'] = '3000'; // kb
        $this->load->library('upload', $config);
        $this->upload->do_upload('c');
        $hasil=$this->upload->data();
        if ($hasil['file_name']==''){
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                            'username'=>$this->session->username,
                            'url'=>$this->input->post('b'),
                            'tgl_posting'=>date('Y-m-d'));
        }else{
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                            'username'=>$this->session->username,
                            'url'=>$this->input->post('b'),
                            'gambar'=>$hasil['file_name'],
                            'tgl_posting'=>date('Y-m-d'));
        }
        $this->model_app->insert('slider',$data);  
        redirect($this->uri->segment(1).'/iklanatas');
    }else{
        $this->template->load('administrator/template','administrator/mod_iklanatas/view_iklanatas_tambah');
    }
}

function edit_iklanatas(){
    cek_session_akses('iklanatas',$this->session->id_session);
    $id = $this->uri->segment(3);
    if (isset($_POST['submit'])){
        $config['upload_path'] = 'assets/img/slider';
        $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
        $config['max_size'] = '3000'; // kb
        $this->load->library('upload', $config);
        $this->upload->do_upload('c');
        $hasil=$this->upload->data();
        if ($hasil['file_name']==''){
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                            'username'=>$this->session->username,
                            'url'=>$this->input->post('b'),
                            'tgl_posting'=>date('Y-m-d'));
        }else{
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                            'username'=>$this->session->username,
                            'url'=>$this->input->post('b'),
                            'gambar'=>$hasil['file_name'],
                            'tgl_posting'=>date('Y-m-d'));
        }
        $where = array('id_slider' => $this->input->post('id'));
        $this->model_app->update('slider', $data, $where);
        redirect($this->uri->segment(1).'/iklanatas');
    }else{
        if ($this->session->level=='admin'){
            $proses = $this->model_app->edit('slider', array('id_slider' => $id))->row_array();
        }else{
            $proses = $this->model_app->edit('slider', array('id_slider' => $id, 'username' => $this->session->username))->row_array();
        }
        $data = array('rows' => $proses);
        $this->template->load('administrator/template','administrator/mod_iklanatas/view_iklanatas_edit',$data);
    }
}

function delete_iklanatas(){
    cek_session_akses('iklanatas',$this->session->id_session);
    if ($this->session->level=='admin'){
        $id = array('id_slider' => $this->uri->segment(3));
    }else{
        $id = array('id_slider' => $this->uri->segment(3), 'username'=>$this->session->username);
    }
    $this->model_app->delete('slider',$id);
    redirect($this->uri->segment(1).'/iklanatas');
}

    function reset_password(){
        if (isset($_POST['submit'])){
            $usr = $this->model_app->edit('users', array('id_session' => $this->input->post('id_session')));
            if ($usr->num_rows()>=1){
                if ($this->input->post('a')==$this->input->post('b')){
                    $data = array('password'=>hash("sha512", md5($this->input->post('a'))));
                    $where = array('id_session' => $this->input->post('id_session'));
                    $this->model_app->update('users', $data, $where);

                    $row = $usr->row_array();
                    $this->session->set_userdata('upload_image_file_manager',true);
                    $this->session->set_userdata(array('username'=>$row['username'],
                                       'level'=>$row['level'],
                                       'id_session'=>$row['id_session']));
                    redirect($this->uri->segment(1).'/home');
                }else{
                    $data['title'] = 'Password Tidak sama!';
                    $this->load->view('administrator/view_reset',$data);
                }
            }else{
                $data['title'] = 'Terjadi Kesalahan!';
                $this->load->view('administrator/view_reset',$data);
            }
        }else{
            $this->session->set_userdata(array('id_session'=>$this->uri->segment(3)));
            $data['title'] = 'Reset Password';
            $this->load->view('administrator/view_reset',$data);
        }
    }

    function lupapassword(){
        if (isset($_POST['lupa'])){
            $email = strip_tags($this->input->post('email'));
            $cekemail = $this->model_app->edit('users', array('email' => $email))->num_rows();
            if ($cekemail <= 0){
                $data['title'] = 'Alamat email tidak ditemukan';
                $this->load->view('administrator/view_login',$data);
            }else{
                $iden = $this->model_app->edit('identitas', array('id_identitas' => 1))->row_array();
                $usr = $this->model_app->edit('users', array('email' => $email))->row_array();
                $this->load->library('email');

                $tgl = date("d-m-Y H:i:s");
                $subject      = 'Lupa Password ...';
                $message      = "<html><body>
                                    <table style='margin-left:25px'>
                                        <tr><td>Halo $usr[nama_lengkap],<br>
                                        Seseorang baru saja meminta untuk mengatur ulang kata sandi Anda di <span style='color:red'>$iden[url]</span>.<br>
                                        Klik di sini untuk mengganti kata sandi Anda.<br>
                                        Atau Anda dapat copas (Copy Paste) url dibawah ini ke address Bar Browser anda :<br>
                                        <a href='".base_url().$this->uri->segment(1)."/reset_password/$usr[id_session]'>".base_url().$this->uri->segment(1)."/reset_password/$usr[id_session]</a><br><br>

                                        Tidak meminta penggantian ini?<br>
                                        Jika Anda tidak meminta kata sandi baru, segera beri tahu kami.<br>
                                        Email. $iden[email], No Telp. $iden[no_telp]</td></tr>
                                    </table>
                                </body></html> \n";
                
                $this->email->from($iden['email'], $iden['nama_website']);
                $this->email->to($usr['email']);
                $this->email->cc('');
                $this->email->bcc('');

                $this->email->subject($subject);
                $this->email->message($message);
                $this->email->set_mailtype("html");
                $this->email->send();
                
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                $data['title'] = 'Password terkirim ke '.$usr['email'];
                $this->load->view('administrator/view_login',$data);
            }
        }else{
            redirect($this->uri->segment(1));
        }
    }

	function home(){
        if ($this->session->level=='admin'){
		  $this->template->load('administrator/template','administrator/view_home_admin');
        }else{
          $data['users'] = $this->model_app->view_where('users',array('username'=>$this->session->username))->row_array();
          $data['modul'] = $this->model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
          $this->template->load('administrator/template','administrator/view_home_users',$data);
        }
	}
   
    
    // Controller Modul Logo

    function logowebsite(){
        cek_session_akses('logowebsite',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/logo/';
            $config['allowed_types'] = 'gif|jpg|png|JPG';
            $config['max_size'] = '2000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('logo');
            $hasil=$this->upload->data();
            $datadb = array('gambar'=>$hasil['file_name']);
            $where = array('id_logo' => $this->input->post('id'));
            $this->model_app->update('logo', $datadb, $where);
            redirect($this->uri->segment(1).'/logowebsite');
        }else{
            $data['record'] = $this->model_app->view('logo');
            $this->template->load('administrator/template','administrator/mod_logowebsite/view_logowebsite',$data);
        }
    }


    // Controller Modul Template Website

    function templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('templates','id_templates','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('templates',array('username'=>$this->session->username),'id_templates','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_template/view_template',$data);
    }

    function tambah_templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        if (isset($_POST['submit'])){
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                'username'=>$this->session->username,
                                'pembuat'=>$this->input->post('b'),
                                'folder'=>$this->input->post('c'));
            $this->model_app->insert('templates',$data);
            redirect($this->uri->segment(1).'/templatewebsite');
        }else{
            $this->template->load('administrator/template','administrator/mod_template/view_template_tambah');
        }
    }

    function edit_templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                'username'=>$this->session->username,
                                'pembuat'=>$this->input->post('b'),
                                'folder'=>$this->input->post('c'));
            $where = array('id_templates' => $this->input->post('id'));
            $this->model_app->update('templates', $data, $where);
            redirect($this->uri->segment(1).'/templatewebsite');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('templates', array('id_templates' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('templates', array('id_templates' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_template/view_template_edit',$data);
        }
    }

    function aktif_templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        $id = $this->uri->segment(3);
        if ($this->uri->segment(4)=='Y'){ $aktif = 'N'; }else{ $aktif = 'Y'; }

        $data = array('aktif'=>$aktif);
        $where = array('id_templates' => $id);
        $this->model_app->update('templates', $data, $where);

        $dataa = array('aktif'=>'N');
        $wheree = array('id_templates !=' => $id);
        $this->model_app->update('templates', $dataa, $wheree);

        redirect($this->uri->segment(1).'/templatewebsite');
    }

    function delete_templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_templates' => $this->uri->segment(3));
        }else{
            $id = array('id_templates' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('templates',$id);
        redirect($this->uri->segment(1).'/templatewebsite');
    }

  	// Controller Modul Download

	function download(){
		cek_session_akses('download',$this->session->id_session);
		$data['record'] = $this->model_app->view_ordering('download','id_download','DESC');
		$this->template->load('administrator/template','administrator/mod_download/view_download',$data);
	}

	function tambah_download(){
		cek_session_akses('download',$this->session->id_session);
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'asset/files/';
            $config['allowed_types'] = 'gif|jpg|png|zip|rar|pdf|doc|docx|ppt|pptx|xls|xlsx|txt';
            $config['max_size'] = '25000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                    $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                    'tgl_posting'=>date('Y-m-d'),
                                    'hits'=>'0');
            }else{
                    $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_file'=>$hasil['file_name'],
                                    'tgl_posting'=>date('Y-m-d'),
                                    'hits'=>'0');
            }
            $this->model_app->insert('download',$data);
			redirect($this->uri->segment(1).'/download');
		}else{
			$this->template->load('administrator/template','administrator/mod_download/view_download_tambah');
		}
	}

	function edit_download(){
		cek_session_akses('download',$this->session->id_session);
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'asset/files/';
            $config['allowed_types'] = 'gif|jpg|png|zip|rar|pdf|doc|docx|ppt|pptx|xls|xlsx|txt';
            $config['max_size'] = '25000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                    $data = array('judul'=>$this->db->escape_str($this->input->post('a')));
            }else{
                    $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_file'=>$hasil['file_name']);
            }
            $where = array('id_download' => $this->input->post('id'));
            $this->model_app->update('download', $data, $where);
			redirect($this->uri->segment(1).'/download');
		}else{
			$proses = $this->model_app->edit('download', array('id_download' => $id))->row_array();
            $data = array('rows' => $proses);
			$this->template->load('administrator/template','administrator/mod_download/view_download_edit',$data);
		}
	}

	function delete_download(){
        cek_session_akses('download',$this->session->id_session);
		$id = array('id_download' => $this->uri->segment(3));
        $this->model_app->delete('download',$id);
		redirect($this->uri->segment(1).'/download');
	}

    // Controller Modul Sekilas Info

    function sekilasinfo(){
        cek_session_akses('sekilasinfo',$this->session->id_session);
        $data['record'] = $this->model_app->view_ordering('sekilasinfo','id_sekilas','DESC');
        $this->template->load('administrator/template','administrator/mod_sekilasinfo/view_sekilasinfo',$data);
    }

    function tambah_sekilasinfo(){
        cek_session_akses('sekilasinfo',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_info/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '2500'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('info'=>$this->input->post('a'),
                              'tgl_posting'=>date('Y-m-d'),
                              'aktif'=>'Y');
            }else{
                $data = array('info'=>$this->input->post('a'),
                              'tgl_posting'=>date('Y-m-d'),
                              'gambar'=>$hasil['file_name'],
                              'aktif'=>'Y');
            }
            $this->model_app->insert('sekilasinfo',$data);
            redirect($this->uri->segment(1).'/sekilasinfo');
        }else{
            $this->template->load('administrator/template','administrator/mod_sekilasinfo/view_sekilasinfo_tambah');
        }
    }

    function edit_sekilasinfo(){
        cek_session_akses('sekilasinfo',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_info/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '2500'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('info'=>$this->input->post('a'),
                              'aktif'=>$this->input->post('f'));
            }else{
                $data = array('info'=>$this->input->post('a'),
                              'gambar'=>$hasil['file_name'],
                              'aktif'=>$this->input->post('f'));
            }

            $where = array('id_sekilas' => $this->input->post('id'));
            $this->model_app->update('sekilasinfo', $data, $where);
            redirect($this->uri->segment(1).'/sekilasinfo');
        }else{
            $proses = $this->model_app->edit('sekilasinfo', array('id_sekilas' => $id))->row_array();
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_sekilasinfo/view_sekilasinfo_edit',$data);
        }
    }

    function delete_sekilasinfo(){
        cek_session_akses('sekilasinfo',$this->session->id_session);
        $id = array('id_sekilas' => $this->uri->segment(3));
        $this->model_app->delete('sekilasinfo',$id);
        redirect($this->uri->segment(1).'/sekilasinfo');
    }



    // Controller Modul Alamat

    function alamat(){
        cek_session_akses('alamat',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $data = array('alamat'=>$this->input->post('a'));
            $where = array('id_alamat' => 1);
            $this->model_app->update('mod_alamat', $data, $where);
            redirect($this->uri->segment(1).'/alamat');
        }else{
            $proses = $this->model_app->edit('mod_alamat', array('id_alamat' => 1))->row_array();
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_alamat/view_alamat',$data);
        }
    }


	// Controller Modul Pesan Masuk

	function pesanmasuk(){
		cek_session_akses('pesanmasuk',$this->session->id_session);
		$data['record'] = $this->model_app->view_ordering('hubungi','id_hubungi','DESC');
		$this->template->load('administrator/template','administrator/mod_pesanmasuk/view_pesanmasuk',$data);
	}

	function detail_pesanmasuk(){
		cek_session_akses('pesanmasuk',$this->session->id_session);
		$id = $this->uri->segment(3);
		$this->db->query("UPDATE hubungi SET dibaca='Y' where id_hubungi='$id'");
		if (isset($_POST['submit'])){
			$nama           = $this->input->post('a');
            $email           = $this->input->post('b');
            $subject         = $this->input->post('c');
            $message         = $this->input->post('isi')." <br><hr><br> ".$this->input->post('d');
            
            $this->email->from('robby.prihandaya@gmail.com', 'PHPMU.COM');
            $this->email->to($email);
            $this->email->cc('');
            $this->email->bcc('');

            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->set_mailtype("html");
            $this->email->send();
            
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

			$proses = $this->model_app->edit('hubungi', array('id_hubungi' => $id))->row_array();
            $data = array('rows' => $proses);
			$this->template->load('administrator/template','administrator/mod_pesanmasuk/view_pesanmasuk_detail',$data);
		}else{
			$proses = $this->model_app->edit('hubungi', array('id_hubungi' => $id))->row_array();
            $data = array('rows' => $proses);
			$this->template->load('administrator/template','administrator/mod_pesanmasuk/view_pesanmasuk_detail',$data);
		}
	}

	function delete_pesanmasuk(){
        cek_session_akses('pesanmasuk',$this->session->id_session);
		$id = array('id_hubungi' => $this->uri->segment(3));
        $this->model_app->delete('hubungi',$id);
		redirect($this->uri->segment(1).'/pesanmasuk');
	}


	// Controller Modul User

	function manajemenuser(){
		cek_session_akses('manajemenuser',$this->session->id_session);
		$data['record'] = $this->model_app->view_ordering('users','username','DESC');
		$this->template->load('administrator/template','administrator/mod_users/view_users',$data);
	}

	function tambah_manajemenuser(){
		cek_session_akses('manajemenuser',$this->session->id_session);
		$id = $this->session->username;
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'asset/foto_user/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '1000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('f');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=>md5($this->input->post('b')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'level'=>$this->db->escape_str($this->input->post('g')),
                                    'blokir'=>'N',
                                    'id_session'=>md5($this->input->post('a')).'-'.date('YmdHis'));
            }else{
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=>md5($this->input->post('b')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name'],
                                    'level'=>$this->db->escape_str($this->input->post('g')),
                                    'blokir'=>'N',
                                    'id_session'=>md5($this->input->post('a')).'-'.date('YmdHis'));
            }
            $this->model_app->insert('users',$data);

              $mod=count($this->input->post('modul'));
              $modul=$this->input->post('modul');
              $sess = md5($this->input->post('a')).'-'.date('YmdHis');
              for($i=0;$i<$mod;$i++){
                $datam = array('id_session'=>$sess,
                              'id_modul'=>$modul[$i]);
                $this->model_app->insert('users_modul',$datam);
              }

			redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->input->post('a'));
		}else{
            $proses = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
            $data = array('record' => $proses);
			$this->template->load('administrator/template','administrator/mod_users/view_users_tambah',$data);
		}
	}

	function edit_manajemenuser(){
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'asset/foto_user/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '1000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('f');
            $hasil=$this->upload->data();
            if ($hasil['file_name']=='' AND $this->input->post('b') ==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'blokir'=>'N');
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') ==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name'],
                                    'blokir'=>'N');
            }elseif ($hasil['file_name']=='' AND $this->input->post('b') !=''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=> md5($this->input->post('b')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'blokir'=>'N');
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') !=''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=> md5($this->input->post('b')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name'],
                                    'blokir'=>'N');
            }
            $where = array('username' => $this->input->post('id'));
            $this->model_app->update('users', $data, $where);

              $mod=count($this->input->post('modul'));
              $modul=$this->input->post('modul');
              for($i=0;$i<$mod;$i++){
                $datam = array('id_session'=>$this->input->post('ids'),
                              'id_modul'=>$modul[$i]);
                $this->model_app->insert('users_modul',$datam);
              }

			redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->input->post('id'));
		}else{
            if ($this->session->username==$this->uri->segment(3) OR $this->session->level=='admin'){
                $proses = $this->model_app->edit('users', array('username' => $id))->row_array();
                $akses = $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
                $data = array('rows' => $proses, 'record' => $modul, 'akses' => $akses);
    			$this->template->load('administrator/template','administrator/mod_users/view_users_edit',$data);
            }else{
                redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->session->username);
            }
		}
	}

	function delete_manajemenuser(){
        cek_session_akses('manajemenuser',$this->session->id_session);
		$id = array('username' => $this->uri->segment(3));
        $this->model_app->delete('users',$id);
		redirect($this->uri->segment(1).'/manajemenuser');
	}

    function delete_akses(){
        cek_session_admin();
        $id = array('id_umod' => $this->uri->segment(3));
        $this->model_app->delete('users_modul',$id);
        redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->uri->segment(4));
    }

	

	// Controller Modul Modul

	function manajemenmodul(){
		cek_session_akses('manajemenmodul',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('modul','id_modul','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('modul',array('username'=>$this->session->username),'id_modul','DESC');
        }
		$this->template->load('administrator/template','administrator/mod_modul/view_modul',$data);
	}

	function tambah_manajemenmodul(){
		cek_session_akses('manajemenmodul',$this->session->id_session);
		if (isset($_POST['submit'])){
			$data = array('nama_modul'=>$this->db->escape_str($this->input->post('a')),
                        'username'=>$this->session->username,
                        'link'=>$this->db->escape_str($this->input->post('b')),
                        'static_content'=>'',
                        'gambar'=>'',
                        'publish'=>$this->db->escape_str($this->input->post('c')),
                        'status'=>$this->db->escape_str($this->input->post('e')),
                        'aktif'=>$this->db->escape_str($this->input->post('d')),
                        'urutan'=>'0',
                        'link_seo'=>'');
            $this->model_app->insert('modul',$data);
			redirect($this->uri->segment(1).'/manajemenmodul');
		}else{
			$this->template->load('administrator/template','administrator/mod_modul/view_modul_tambah');
		}
	}

	function edit_manajemenmodul(){
		cek_session_akses('manajemenmodul',$this->session->id_session);
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
            $data = array('nama_modul'=>$this->db->escape_str($this->input->post('a')),
                        'username'=>$this->session->username,
                        'link'=>$this->db->escape_str($this->input->post('b')),
                        'static_content'=>'',
                        'gambar'=>'',
                        'publish'=>$this->db->escape_str($this->input->post('c')),
                        'status'=>$this->db->escape_str($this->input->post('e')),
                        'aktif'=>$this->db->escape_str($this->input->post('d')),
                        'urutan'=>'0',
                        'link_seo'=>'');
            $where = array('id_modul' => $this->input->post('id'));
            $this->model_app->update('modul', $data, $where);
			redirect($this->uri->segment(1).'/manajemenmodul');
		}else{
            if ($this->session->level=='admin'){
                 $proses = $this->model_app->edit('modul', array('id_modul' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('modul', array('id_modul' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
			$this->template->load('administrator/template','administrator/mod_modul/view_modul_edit',$data);
		}
	}

	function delete_manajemenmodul(){
        cek_session_akses('manajemenmodul',$this->session->id_session);
		if ($this->session->level=='admin'){
            $id = array('id_modul' => $this->uri->segment(3));
        }else{
            $id = array('id_modul' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('modul',$id);
		redirect($this->uri->segment(1).'/manajemenmodul');
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('/');
	}
}
